#!/bin/bash
# variables.sh: common functions

# we get project and branch as environment variables set by gitlab ci
PROJECT="$CI_PROJECT_NAME"
BRANCH="$CI_COMMIT_REF_NAME"
NAMESPACE="$CI_PROJECT_NAMESPACE"
ENVIRONMENT="$CI_ENVIRONMENT_NAME"
COMMIT="$CI_COMMIT_SHA"

# our variables
DEPLOY_MODE="rsync"
RSYNC_DEBUG_PARAMS=""
